const supertest = require('supertest');
const app = require('./index');
const AWS = require('aws-sdk');

const baseUrl = process.env.APP_JEST_URL;

const OLD_ENV = process.env;

beforeEach((done) => {
    
    AWS.config.update({
        region: process.env.AWS_REGION
    });
    jest.setTimeout(10000)
   
    done()
})


describe('GET /', () => {
    test('Get Data in productos', async () => {
        await supertest(app)
            .get('/')
            .expect(200);

    });
});


